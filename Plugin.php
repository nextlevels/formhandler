<?php namespace Nextlevels\Formhandler;

use Nextlevels\Formhandler\Components\Forms;
use System\Classes\PluginBase;

/**
 * Class Plugin
 *
 * @author Jan Malte Kirsten <jan.kirsten@next-levels.de>, Next Levels GmbH
 */
class Plugin extends PluginBase
{
    /**
     * @return array
     */
    public function registerComponents(): array
    {
        return [
            Forms::class => 'fhForm'
        ];
    }
}
