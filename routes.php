<?php

use Fruitcake\Cors\HandleCors;

Route::middleware(HandleCors::class)
    ->prefix('api')
    ->namespace('Nextlevels\Formhandler\Http\Controllers')
    ->group(function () {
        Route::post('upload-file', 'FileController@uploadFile');
    });
