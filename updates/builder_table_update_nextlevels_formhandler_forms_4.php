<?php namespace Nextlevels\Formhandler\Updates;

use October\Rain\Database\Updates\Migration;
use Schema;

class BuilderTableUpdateNextlevelsFormhandlerForms4 extends Migration
{
    public function up()
    {
        Schema::table('nextlevels_formhandler_forms', function ($table) {
            $table->boolean('attachment_for_user')->default(0);
        });
    }

    public function down()
    {
        Schema::table('nextlevels_formhandler_forms', function ($table) {
            $table->dropColumn('attachment_for_user');
        });
    }
}
