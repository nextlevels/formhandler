<?php namespace Nextlevels\Formhandler\Updates;

use October\Rain\Database\Updates\Migration;
use Schema;

class BuilderTableUpdateNextlevelsFormhandlerRequests extends Migration
{
    public function up()
    {
        Schema::table('nextlevels_formhandler_requests', function ($table) {
            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->nullable();
        });
    }

    public function down()
    {
        Schema::table('nextlevels_formhandler_requests', function ($table) {
            $table->dropColumn('created_at');
            $table->dropColumn('updated_at');
        });
    }
}
