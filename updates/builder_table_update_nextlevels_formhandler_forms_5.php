<?php namespace Nextlevels\Formhandler\Updates;

use October\Rain\Database\Updates\Migration;
use Schema;

class BuilderTableUpdateNextlevelsFormhandlerForms5 extends Migration
{
    public function up()
    {
        Schema::table('nextlevels_formhandler_forms', function ($table) {
            $table->string('mail_to');
        });
    }

    public function down()
    {
        Schema::table('nextlevels_formhandler_forms', function ($table) {
            $table->dropColumn('mail_to');
        });
    }
}
