<?php namespace Nextlevels\Formhandler\Updates;

use October\Rain\Database\Updates\Migration;
use Schema;

class BuilderTableUpdateNextlevelsFormhandlerForms2 extends Migration
{
    public function up()
    {
        Schema::table('nextlevels_formhandler_forms', function ($table) {
            $table->text('user_template')->nullable();
        });
    }

    public function down()
    {
        Schema::table('nextlevels_formhandler_forms', function ($table) {
            $table->dropColumn('user_template');
        });
    }
}
