<?php namespace Nextlevels\Formhandler\Updates;

use October\Rain\Database\Updates\Migration;
use Schema;

class BuilderTableCreateNextlevelsFormhandlerForms extends Migration
{
    public function up()
    {
        Schema::create('nextlevels_formhandler_forms', function ($table) {
            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned();
            $table->string('name');
            $table->text('rules');
            $table->string('master_mail');
            $table->text('template')->nullable();
            $table->boolean('send_answer')->default(0);
            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->nullable();
        });
    }

    public function down()
    {
        Schema::dropIfExists('nextlevels_formhandler_forms');
    }
}
