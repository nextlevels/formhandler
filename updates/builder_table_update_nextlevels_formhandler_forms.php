<?php namespace Nextlevels\Formhandler\Updates;

use October\Rain\Database\Updates\Migration;
use Schema;

class BuilderTableUpdateNextlevelsFormhandlerForms extends Migration
{
    public function up()
    {
        Schema::table('nextlevels_formhandler_forms', function ($table) {
            $table->text('rules')->nullable()->change();
        });
    }

    public function down()
    {
        Schema::table('nextlevels_formhandler_forms', function ($table) {
            $table->text('rules')->nullable(false)->change();
        });
    }
}
