<?php namespace Nextlevels\Formhandler\Updates;

use October\Rain\Database\Updates\Migration;
use Schema;

class BuilderTableCreateNextlevelsFormhandlerRequests extends Migration
{
    public function up()
    {
        Schema::create('nextlevels_formhandler_requests', function ($table) {
            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned();
            $table->string('name');
            $table->text('fields');
        });
    }

    public function down()
    {
        Schema::dropIfExists('nextlevels_formhandler_requests');
    }
}
