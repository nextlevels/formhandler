<?php namespace Nextlevels\Formhandler\Models;

use Model;
use Nextlevels\Formhandler\Helpers\FormFieldsHelper;
use October\Rain\Database\Traits\Validation;
use RainLab\Translate\Behaviors\TranslatableModel;

/**
 * Class Form
 *
 * @author Jan Malte Kirsten <jan.kirsten@next-levels.de>, Next Levels GmbH
 */
class Form extends Model
{
    use Validation;

    /**
     * @var string The database table used by the model.
     */
    public $table = 'nextlevels_formhandler_forms';

    /**
     * @var array
     */
    protected $rules = [
        'master_mail' => 'required',
        'name' => 'required|unique:nextlevels_formhandler_forms,name',
        'template' => 'required',
        'subject' => 'required',
        'user_template' => 'required_if:send_answer,true',
        'user_subject' => 'required_if:send_answer,true',
        'mail_to' => 'required_if:send_answer,true'
    ];

    /**
     * @var array
     */
    protected $customeAttributes = [
        'master_mail' => 'nextlevels.formhandler::lang.model.form.fields.master_mail',
        'name' => 'nextlevels.formhandler::lang.model.form.fields.name',
        'template' => 'nextlevels.formhandler::lang.model.form.fields.template',
        'subject' => 'nextlevels.formhandler::lang.model.form.fields.subject',
        'user_template' => 'nextlevels.formhandler::lang.model.form.fields.user_template',
        'user_subject' => 'nextlevels.formhandler::lang.model.form.fields.user_subject',
        'mail_to' => 'nextlevels.formhandler::lang.model.form.fields.mail_to'
    ];

    /**
     * @var array
     */
    public $implement = ['@' . TranslatableModel::class];

    /**
     * @var array
     */
    public $translatable = [
        'template',
        'user_template',
        'user_subject',
        'subject',
        'rules',
        'mail_to'
    ];

    /**
     * @var array
     */
    protected $jsonable = ['rules'];

    /**
     *
     * @return array
     */
    public function getFieldTypeOptions(): array
    {
        return [
            'input' => 'nextlevels.formhandler::lang.model.form.fields.rule.field_types.input',
            'textarea' => 'nextlevels.formhandler::lang.model.form.fields.rule.field_types.textarea',
            'select' => 'nextlevels.formhandler::lang.model.form.fields.rule.field_types.select',
            'media' => 'nextlevels.formhandler::lang.model.form.fields.rule.field_types.media',
            'submit_button' => 'nextlevels.formhandler::lang.model.form.fields.rule.field_types.submit_button'
        ];
    }

    /**
     * Before save
     */
    public function beforeSave(): void
    {
        $this->frontend_template = FormFieldsHelper::renderForm($this);
    }
}
