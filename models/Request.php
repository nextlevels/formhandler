<?php namespace Nextlevels\Formhandler\Models;

use Model;
use System\Models\File;

/**
 * Class Request
 *
 * @author Jan Malte Kirsten <jan.kirsten@next-levels.de>, Next Levels GmbH
 */
class Request extends Model
{
    /**
     * @var string The database table used by the model.
     */
    public $table = 'nextlevels_formhandler_requests';

    /**
     * @var array
     */
    protected $jsonable = ['fields'];

    /**
     * @var array
     */
    public $attachMany = ['media' => File::class];
}
