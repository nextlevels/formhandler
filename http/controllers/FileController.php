<?php

namespace Nextlevels\Formhandler\Http\Controllers;

use Illuminate\Routing\Controller;
use System\Models\File;

/**
 * Class FileController
 *
 * @author Jan Malte Kirsten <jan.kirsten@next-levels.de>, Next Levels GmbH
 */
class FileController extends Controller
{
    /**
     * Upload Handler
     */
    public function uploadFile()
    {
        $validator = \Validator::make(
            $form = \Input::all(),
            [
                'image' => 'required|mimes:jpeg,jpg,png,svg,img,pdf,doc,docx|max:20000'
            ],
            [
                'image' => 'nextlevels.formhandler::lang.file_controller.image'
            ]
        );

        if ($validator->fails()) {
            throw new \ValidationException($validator);
        }

        return (new File())->fromPost($form['image'])->getPath();
    }
}
