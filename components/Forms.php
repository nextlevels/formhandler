<?php namespace Nextlevels\Formhandler\Components;

use Cms\Classes\ComponentBase;
use Illuminate\Http\RedirectResponse;
use Nextlevels\Formhandler\Helpers\RequestHelper;

/**
 * Class Forms
 *
 * @author Jan Malte Kirsten <jan.kirsten@next-levels.de>, Next Levels GmbH
 */
class Forms extends ComponentBase
{
    /**
     * @return array
     */
    public function componentDetails(): array
    {
        return [
            'name' => 'nextlevels.formhandler::lang.model.form.label',
            'description' => 'nextlevels.formhandler::lang.model.form.description'
        ];
    }

    /**
     * On send request
     */
    public function onSendRequest(): RedirectResponse
    {
        return (new  RequestHelper)->handleRequest(\Input::all());
    }
}
