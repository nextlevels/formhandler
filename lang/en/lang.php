<?php return [
    'plugin' => [
        'name' => 'FormHandler',
        'description' => '',
    ],
    'model' => [
        'form' => [
            'label' => 'Formhandler',
            'label_model' => 'Formular',
            'label_plural' => 'Formulare',
            'description' => 'Manage Formulare',
            'name' => 'Name',
            'fields' => [
                'name' => 'Name',
                'master_mail' => 'Absender E-Mail',
                'template' => 'Admin Mail Template',
                'rules' => 'Felder und Regeln',
                'rule' => [
                    'field_name' => 'Feld Name',
                    'rule' => 'Regeln (mit | getrennt)',
                    'field_type' => 'Feld Typ',
                    'display_name' => 'Anzeige Name',
                    'prompt' => 'Neues Feld hinzufügen',
                    'field_position' => 'Feld Position',
                    'class' => 'CSS Klassen',
                    'placeholder' => 'Placeholder',
                    'field_types' => [
                        'input' => 'Input',
                        'textarea' => 'Textarea',
                        'select' => 'Select',
                        'media' => 'Media',
                        'submit_button' => 'Senden Button',
                    ]
                ],
                'send_answer' => 'Antwort an Benutzer senden?',
                'user_template' => 'Benutzer Template',
                'user_subject' => 'Benutzer Betreff',
                'subject' => 'Admin Betreff',
                'attachment_for_user' => 'Mögliche Anhänge auch dem Nutzer senden',
                'mail_to' => 'Benutzer Mail',
                'frontend-template' => 'Template',
                'render_frontend_template' => 'Neues Formular Template generieren',
                'count_columns' => 'Anzahl Spalten',
                'formular_classes' => 'Formular Klassen',
            ],
            'tabs' => [
                'template' => 'Template',
                'settings' => 'Einstellungen',
                'user-settings' => 'Benutzer Einstellungen',
                'media' => 'Medien',
                'frontend_template' => 'Formular Template',
            ],
            'hint' => 'In diesem Tab können Sie alle Werte aus dem Formular des Users mithilfe des Feldnamens und geschweiften Klammern abrufen und verwenden. Z.B. {name}',
            'hint_content' => 'Es stehen Ihnen folgende Attribute zur Verfügung',
            'hint_no_keys' => 'Noch kein Attribut vorhanden'
        ],
        'general' => [
            'id' => 'ID',
            'updated_at' => 'Geändert am',
            'created_at' => 'Erstellt am'
        ],
        'request' => [
            'label' => 'Anfrage',
            'label_plural' => 'Anfragen',
            'fields' => [
                'name' => 'Name',
                'fields' => 'Felder',
                'field' => [
                    'name' => 'Feld Name',
                    'value' => 'Feld Inhalt',
                ],
                'details' => 'Details',
                'media' => 'Dateien',
            ],
            'menu' => [
                'name' => 'Anfragen',
            ],
        ],
        'requests' => [
            'tabs' => 'Felder',
        ],
    ],
    'form' => [
        'menu' => [
            'name' => 'Formular',
        ],
    ],
    'mail' => [
        'new-request' => 'Neue Anfrage (Formular %s)',
        'success' => 'Erfolgreich abgesendet',
        'error' => 'Ein Fehler ist aufgetreten, die Seite wurde neu geladen!',
    ],
    'file_controller' => [
        'image' => 'Bild'
    ]
];
