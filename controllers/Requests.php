<?php namespace Nextlevels\Formhandler\Controllers;

use Backend\Behaviors\FormController;
use Backend\Behaviors\ListController;
use Backend\Classes\Controller;
use BackendMenu;

/**
 * Class Requests
 *
 * @author Jan Malte Kirsten <jan.kirsten@next-levels.de>, Next Levels GmbH
 */
class Requests extends Controller
{
    /**
     * @var array
     */
    public $implement = [ListController::class, FormController::class];

    /**
     * @var string
     */
    public $listConfig = 'config_list.yaml';
    public $formConfig = 'config_form.yaml';

    /**
     * Requests constructor.
     */
    public function __construct()
    {
        parent::__construct();
        BackendMenu::setContext('Nextlevels.Formhandler', 'formhandler', 'formhandler-requests');
    }
}
