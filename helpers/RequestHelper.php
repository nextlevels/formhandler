<?php namespace Nextlevels\Formhandler\Helpers;

use Illuminate\Http\RedirectResponse;
use Nextlevels\Formhandler\Models\Form;
use Nextlevels\Formhandler\Models\Request;
use October\Rain\Parse\Bracket;
use System\Models\File;
use October\Rain\Support\Facades\Flash;

/**
 * Class FormHelper
 *
 * @author Jan Malte Kirsten <jan.kirsten@next-levels.de>, Next Levels GmbH
 */
class RequestHelper
{
    /**
     * @var array
     */
    protected $rules = [];
    protected $customAttributes = [];
    protected $handlerData = [];
    protected $data = [];
    protected $attachments = [];
    protected $form;

    /**
     * Set All Data
     */
    protected function setAllData(): void
    {
        foreach ($this->form->rules as $rule) {
            $this->rules[$rule['field_name']] = $rule['field_rule'];
            $this->customAttributes[$rule['field_name']] = $rule['field_display_name'];

            if ($rule['field_type'] !== 'none') {
                $this->handlerData[$rule['field_type']] = $rule['field_name'];
            }
        }
    }

    /**
     * Set remaining data
     */
    protected function setRemainingData(): void
    {
        foreach ($this->handlerData as $key => $handlerDataValue) {
            $this->handlerData[$key] = $this->data[$handlerDataValue];
        }
    }

    /**
     * Handle ux Answer
     */
    protected function handleUserMail(): void
    {
        \Mail::raw($this->parseTemplate($this->form->user_template, $this->data), function ($message) {
            $message->to($this->parseTemplate($this->form->mail_to, $this->data));
            $message->subject(
                $this->parseTemplate($this->form->user_subject, $this->data)
            );

            if ($this->form->attachment_for_user) {
                foreach ($this->attachments as $key => $attachment) {
                    $message->attach(
                        storage_path(str_replace('/storage/', '', parse_url($attachment)['path'])),
                        ['as' => 'Media' . $key . '.' . pathinfo($attachment, PATHINFO_EXTENSION)]
                    );
                }
            }
        });
    }

    /**
     * Handle Admin Mail
     */
    protected function handleAdminMail(): void
    {
        \Mail::raw($this->parseTemplate($this->form->template, $this->data), function ($message) {
            $message->to($this->form->master_mail);
            $message->subject(
                $this->parseTemplate($this->form->subject, $this->data)
            );
            foreach ($this->attachments as $key => $attachment) {
                $message->attach(
                    storage_path(str_replace('/storage/', '', parse_url($attachment)['path'])),
                    ['as' => 'Media' . $key . '.' . pathinfo($attachment, PATHINFO_EXTENSION)]
                );
            }
        });
    }

    /**
     * Save data to request
     */
    protected function saveToRequests(): void
    {
        $request = new Request();
        $request->name = $this->form->name . ' ' . date('M,d,Y h:i:s A');

        foreach ($this->data as $key => $dataSet) {
            if ($key !== 'formName') {
                $fields[] = ['fields_name' => $this->customAttributes[$key], 'fields_value' => $dataSet];
            }
        }
        $request->fields = $fields;
        $request->save();

        foreach ($this->attachments as $key => $attachment) {
            $attachmentPath = parse_url($attachment)['path'];
            $filePath = storage_path(str_replace('/storage/', '', $attachmentPath));
            $request->media()->add((new File())->fromFile($filePath));
        }
    }

    /**
     * @param $data
     * @return RedirectResponse
     * @throws \ValidationException
     */
    public function handleRequest($data): RedirectResponse
    {
        if (isset($data['formName']) && ($this->form = Form::where('name', $data['formName'])->first()) !== null) {
            $this->setAllData();

            $validator = \Validator::make($data, $this->rules, [], $this->customAttributes);
            if ($validator->fails()) {
                throw new \ValidationException($validator);
            }

            $this->data = $data;
            $this->setRemainingData();

            if (array_key_exists('media', $this->handlerData)) {
                $this->attachments = array_filter(explode(';', $this->handlerData['media']));
            }
        } else {
        Flash::error(\Lang::get('nextlevels.formhandler::lang.mail.error'));
        return \Redirect::refresh();
        }

        $this->handleAdminMail();

        if ($this->form->send_answer) {
            $this->handleUserMail();
        }

        $this->saveToRequests();

        Flash::success(\Lang::get('nextlevels.formhandler::lang.mail.success'));
        return \Redirect::refresh();
    }

    /**
     * @param $template
     * @param $variables
     * @return Bracket
     */
    public function parseTemplate($template, $variables): string
    {
        return Bracket::parse($template, $variables);
    }
}
