<?php namespace Nextlevels\Formhandler\Helpers;

use October\Rain\Parse\Bracket;

/**
 * Class FormFieldsHelper
 *
 * @author Jan Kirsten <jan.kirsten@next-levels.de>, Next Levels GmbH
 */
class FormFieldsHelper
{
    /**
     * @var array
     */
    public static $formTypes = [
        'input' => '<input class="{field_class}" name="{field_name}" type="text" placeholder="{field_placeholder}">',
        'textarea' => '<textarea class="{field_class}" rows="5" name="{field_name}" placeholder="{field_placeholder}"></textarea>',
        'select',
        'media'
    ];

    /**
     * @param $form
     * @return string
     */
    public static function renderForm($form): string
    {
        $rules = json_decode($form->attributes['rules'], true);

        $frontendForm = '<form class="form-footer-contact">';
        $frontendForm .= self::renderFields($rules);
        $frontendForm .= '<a data-request="onSendRequest" data-request-data="formName: \'' . $form->name . '\'" class="btn-arrow uk-margin-large-top"  data-attach-loading data-request-validate data-request-flash></a></form>';

        return $frontendForm;
    }

    /**
     * @param $fields
     * @return string
     */
    public static function renderFields($fields): string
    {
        $formTypes = static::$formTypes;
        $returnFields = '';

        foreach ($fields as $field) {
            $returnFields .= Bracket::parse(array_get($formTypes, $field['field_type']), $field);
        }
        return $returnFields;
    }
}
